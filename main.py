from functools import partial
import os
import os.path as osp
import time
import multiprocessing

from tkinter import *
from tkinter import ttk
import sounddevice as sd
import soundfile as sf

BUTTON_SIZE = 100

class App:
    def __init__(self, root):
        #setting title
        root.title("Jinglemaster2000")
        self.root_dir = os.getcwd()
        self.style = ttk.Style(root)
        self.seconds_to_play = 0
        self.count_down_thread = multiprocessing.Process()
        
        self.files = {}
        sound_dir = osp.join(self.root_dir, 'sounds')
        if osp.exists(sound_dir):
            self.files = {n.split('.')[0]: osp.join(sound_dir, n) for n in  sorted(os.listdir('sounds'))}
        
        button_list = []
        cols = int(len(self.files) ** 0.5)
        col, row = 0, 0
        width = 20 + cols * BUTTON_SIZE
        height = ((len(self.files) // cols + (len(self.files) % cols)) * BUTTON_SIZE) + 20 + 60

        screenwidth = root.winfo_screenwidth()
        screenheight = root.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        root.geometry(alignstr)
        root.resizable(width=False, height=False)

        self.button_functions = []
        for i, filename in enumerate(self.files):
            col = i % cols
            row = i // cols
            name = filename.split('.')[0]
            button = Button(root,
                            text=name,
                            font='Arial 10',
                            command=partial(self.on_click_music_button, name, i))
            
            button.place(x=10 + col*BUTTON_SIZE,
                         y=10 + row*BUTTON_SIZE,
                         width=BUTTON_SIZE,
                         height=BUTTON_SIZE)
            button_list.append(button)
        
        self.stop_button = Button(root,
                                  text='Stop',
                                  command=self.on_click_stop_button)
        self.stop_button.place(x=10,
                               y=20 + (row + 1) * BUTTON_SIZE,
                               width=width-20,
                               height=50)
        
    def on_click_music_button(self, name, i):
        if self.count_down_thread.is_alive():
            self.count_down_thread.terminate()
        
        data, samplerate = sf.read(self.files[name], dtype='float32')
        time.sleep(0.1)
        self.seconds_to_play = len(data) // samplerate
        sd.play(data, samplerate)
        self.count_down_thread = multiprocessing.Process(target=count_down, args=(self.seconds_to_play, ))
        self.count_down_thread.start()
    
    def on_click_stop_button(self):
        sd.stop()
        if self.count_down_thread.is_alive():
            self.count_down_thread.terminate()
        
        self.seconds_to_play = 0

def count_down(num):
    while num > 0:
        print(num)
        num -= 1
        time.sleep(1)

if __name__ == "__main__":
    root = Tk()
    app = App(root)
    root.mainloop()
